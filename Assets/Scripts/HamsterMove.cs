﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HamsterMove : MonoBehaviour
{
    //public VibrationController VibrationController;

    public AudioSource PunchSound;

    bool a = false;
    static public bool b = false;

    public float TimeForCombo;
    
    static public int PointsPerHit = 10;

    public Transform PointOutBox;
    public Transform PointInBox;

    [SerializeField]
    SpriteRenderer HamsterColor;
    

    public float Speed;
    public float SpeedAfterClick;

    bool isHamsterCanMove = true;

    [HideInInspector]
    public float startTimer = 0;
    [HideInInspector]
    public float timerAfterClic = 0;

    [SerializeField]
    public UIController UIHamster;

    public float timeSitInBox;
    public float timeSitOutBox;
    public float timeSitInBoxAfterClick;


   // [HideInInspector]
    public HamsterState CurrentHamsterState;

    void Start()
    {
        CurrentHamsterState = HamsterState.hamsterInBox;
        transform.position = PointInBox.transform.position;
    }

    void Update()
    {
       

        if (CurrentHamsterState == HamsterState.hamsterInBox)
        {
            startTimer += Time.deltaTime;
            if (startTimer >= timeSitInBox)
            {
                CurrentHamsterState = HamsterState.hamsterGoUp;
                startTimer = 0;
            }
        }
        if (CurrentHamsterState == HamsterState.hamsterGoUp)
        {
            GoOutBox();
            if (transform.position == PointOutBox.transform.position)
            {
                CurrentHamsterState = HamsterState.hamstertOutBox;
            }
        }
        if (CurrentHamsterState == HamsterState.hamstertOutBox)
        {
            startTimer += Time.deltaTime;
            if (startTimer >= timeSitOutBox)
            {
                startTimer = 0;
                CurrentHamsterState = HamsterState.hamsterGoDown;
            }
        }
        if (CurrentHamsterState == HamsterState.hamsterGoDown)
        {
            GoInBox();
            if (transform.position == PointInBox.transform.position)
            {
                CurrentHamsterState = HamsterState.hamsterInBox;
            }
        }
        if (CurrentHamsterState == HamsterState.hamsterSleep)
        {
            GoInBox();
        }
        if (CurrentHamsterState == HamsterState.hamsterClicked)
        {
            hamsterAfterClic();
            
        }

        if (a == true)
        {
            b = true;
            ComboTimer();
            if (TimeForCombo <= 0)
            {
                b = false;
                PointsPerHit = 10;
                TimeForCombo = 0.5f;
                a = false;
            }
        }
    }

    void GoOutBox()
    {
        if (isHamsterCanMove == true)
            transform.position = Vector3.MoveTowards(transform.position, PointOutBox.position, Speed * Time.deltaTime);
    }

    public void GoInBox()
    {
        if (isHamsterCanMove == true)
            transform.position = Vector3.MoveTowards(transform.position, PointInBox.position, Speed * Time.deltaTime);
    }

    private void OnMouseDown()
    {
        

        if (CurrentHamsterState != HamsterState.hamsterClicked && CurrentHamsterState != HamsterState.hamsterInBox)
        {
            PunchSound.Play();
            a = true;

            //VibrationController.Vibration();

            UIHamster.RecordScore(PointsPerHit);
            HamsterColor.color = new Color(1f, 0.6f, 0.6f, 1f);
            CurrentHamsterState = HamsterState.hamsterClicked;
        }
    }

    private void hamsterAfterClic()
    {
        timeSitInBoxAfterClick = Random.Range(0, 5);
        //Speed = Random.Range(4, 10);

        transform.position = Vector3.MoveTowards(transform.position, PointInBox.position, SpeedAfterClick * Time.deltaTime);
        if (transform.position == PointInBox.transform.position)
        {
            timerAfterClic += Time.deltaTime;

            if (timerAfterClic >= timeSitInBoxAfterClick)
            {
                CurrentHamsterState = HamsterState.hamsterInBox;
                HamsterColor.color = new Color(1f, 1f, 1f, 1f);
                startTimer = 0;
                timerAfterClic = 0;
            }
        }
    }

    public void RestartHamster()
    {
        startTimer = 0;
        timerAfterClic = 0;
        transform.position = PointInBox.transform.position;
        CurrentHamsterState = HamsterState.hamsterInBox;
        HamsterColor.color = new Color(1f, 1f, 1f, 1f);
    }

    public void ComboTimer()
    {
        
        TimeForCombo -= Time.deltaTime; 
       
    }
}




