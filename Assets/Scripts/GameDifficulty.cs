﻿public enum GameDifficulty
{
    Easy = 1,
    Normal = 2,
    Hard = 3
}