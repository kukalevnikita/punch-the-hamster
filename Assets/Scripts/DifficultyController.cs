﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DifficultyController : MonoBehaviour
{
    [HideInInspector]
    public GameDifficulty CurrentGameDifficulty;

    [SerializeField]
    private float TimeWithEasyDifficulty;
    [SerializeField]
    private float TimeWithNormalDifficulty;
    [SerializeField]
    private float TimeWithHardDifficulty;

    public Text DifficultyIndicator;

    //public HamsterMove HamsterMove;
    public TimeController HamsterController;

    void Start()
    {
        CurrentGameDifficulty = GameDifficulty.Normal;
    }

    
    void Update()
    {

        if (CurrentGameDifficulty == GameDifficulty.Easy)
        {
            DifficultyIndicator.text = "easy";
            HamsterController.DayTime = TimeWithEasyDifficulty;

            for (int i = 0; i < HamsterController.SleepHamster.Count; i++)
            {
                HamsterController.SleepHamster[i].Speed = 4;
                HamsterController.SleepHamster[i].TimeForCombo = 2f;
            }
        }

        else if (CurrentGameDifficulty == GameDifficulty.Normal)
        {
            DifficultyIndicator.text = "normal";
            HamsterController.DayTime = TimeWithNormalDifficulty;

            for (int i = 0; i < HamsterController.SleepHamster.Count; i++)
            {
                HamsterController.SleepHamster[i].Speed = 6;
                HamsterController.SleepHamster[i].TimeForCombo = 1f;
            }
        }

        else if (CurrentGameDifficulty == GameDifficulty.Hard)
        {
            DifficultyIndicator.text = "hard";
            HamsterController.DayTime = TimeWithHardDifficulty;

            for (int i = 0; i < HamsterController.SleepHamster.Count; i++)
            {
                HamsterController.SleepHamster[i].Speed = 12;
                HamsterController.SleepHamster[i].TimeForCombo = 0.5f;
            }
        }

    }

    public void IncrementDifficulty()
    {
        try
        {
            CurrentGameDifficulty++;
        }
        catch (Exception NULL)
        {
            CurrentGameDifficulty = GameDifficulty.Normal;
            Console.WriteLine(NULL);
            throw;
        }
        
    }

    public void DecrementDifficulty()
    {
        try
        {
            CurrentGameDifficulty--;
        }
        catch (Exception NULL)
        {
            CurrentGameDifficulty = GameDifficulty.Normal;
            Console.WriteLine(NULL);
            throw;
        }
    }

}
