﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TimeController))]
public class TimeControllerCustomEditor : Editor
{
    private int count;

    private GUIStyle redStyle;

    public override void OnInspectorGUI()
    {
        redStyle = new GUIStyle
        {
            normal = new GUIStyleState
            {
                textColor = Color.red
            }
        };
        TimeController timeController = (TimeController) target;
        serializedObject.Update();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("hamsterMove"), true);

        count = EditorGUILayout.IntField("Count", count);
        if (GUILayout.Button("Fill array"))
        {
            for (int i = 0; i < count; i++)
            {
                timeController.SleepHamster.Add(timeController.hamsterMove);
            }
        }

        if (GUILayout.Button("Clear array"))
        {
            timeController.SleepHamster.Clear();
        }

        if (timeController.SleepHamster.Count == 0)
        {
            GUILayout.Label("Array is empty", redStyle);
        }


        EditorGUILayout.PropertyField(serializedObject.FindProperty("SleepHamster"), true);
        serializedObject.ApplyModifiedProperties();
    }
}