﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundsController : MonoBehaviour
{
    public bool isSoundOn = true;

    public GameObject CrossIcon;

    public GameObject SoundControllerGameObject;

    public AudioSource ButtonSound;

    public AudioSource GameScreenBackgroundSound;
    public AudioSource MainScreenBackgroundSound;

    public AudioListener Audio;

    void Start()
    {
        
    }

    
    void Update()
    {
        
    }

    public  void PlayButtonSound()
    {
        ButtonSound.Play();
    }

    public void PlayMainScreenBackgroundSound()
    {
        GameScreenBackgroundSound.Stop();
        if (!MainScreenBackgroundSound.isPlaying)
        {
            MainScreenBackgroundSound.Play();
        }
    }

    public void PlayGameScreenBackgroundSound()
    {
        GameScreenBackgroundSound.Play();
        MainScreenBackgroundSound.Stop();
    }

    public void OffAudio()
    {
        
        Audio.enabled = false;
        //SoundControllerGameObject.SetActive(false);
    }

    public void OnAudio()
    {
        Audio.enabled = true;
       // SoundControllerGameObject.SetActive(true);
    }

    public void ChangeSoundSetting()
    {
        if (isSoundOn)
        {
            SoundControllerGameObject.SetActive(false);
            isSoundOn = false;
            CrossIcon.SetActive(true);
        }
        else
        {
            SoundControllerGameObject.SetActive(true);
            isSoundOn = true;
            CrossIcon.SetActive(false);
            PlayMainScreenBackgroundSound();
        }
       // SoundControllerGameObject.SetActive(true);
    }

}
