﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public SoundsController SoundsController;

    public GameObject ComboIndicator;
    public GameObject CompleteScreen;
    public GameObject GameScreen;
    public GameObject PauseScreen;
    public GameObject MainScreen;
    public GameObject SettingsScreen;
    public GameObject NewRecordScreen;
    public GameObject LeaderboardScreen;

    public bool isCombo = false;
    public float ShowComboTime = 0;

    public int Score;

    public Text FinalScore;
    public Text CurrentScore;
    public Text GameTimer;
    
    bool isPause = false;

    [SerializeField]
    public TimeController HamsterGoBed;

    void Start ()
    {
        NavigateToTheMainScreen();
    }
	
	
	void Update ()
    {

        if (isCombo == true)
        {
            ShowComboTime += Time.deltaTime;
            if (ShowComboTime >= 1)
            {
                ComboIndicator.SetActive(false);
                ShowComboTime = 0;
                isCombo = false;
            }
        }


        FinalScore.text = "" + Score;
        CurrentScore.text = "Score: " + Score;

        

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPause == false)
            {
                OpenPauseScreen();
                CloseGameScreen();
                Time.timeScale = 0;
                isPause = true;
            }
            else
            {
                ClosePauseScreen();
                OpenGameScreen();
                Time.timeScale = 1;
                isPause = false;
            }
        }


    }
    
    public void RestartTheGame()
    {
        HamsterGoBed.RestartTimer();
        ClosePauseScreen();
        CloseCompleteScreen();
        OpenGameScreen();
        Time.timeScale = 1;
        Score = 0;
        ComboIndicator.SetActive(false);



        for (int i = 0; i < HamsterGoBed.SleepHamster.Count; i++)
        {

            HamsterGoBed.SleepHamster[i].RestartHamster();

        }
    }

    public void StartTheGame()
    {
        SoundsController.PlayGameScreenBackgroundSound();

        Time.timeScale = 1;
        CloseMainScreen();
        OpenGameScreen();
        Score = 0;

        for (int i = 0; i < HamsterGoBed.SleepHamster.Count; i++)
        {

            HamsterGoBed.SleepHamster[i].RestartHamster();

        }
    }

    public void NavigateToTheSettingsScreen()
    {
        CloseMainScreen();
        OpenSettingsScreen();
    }

    public void NavigateToTheLeaderboardScreen()
    {
        CloseMainScreen();
        OpenLeaderboardScreen();
    }

    public void NavigateToTheMainScreen()
    {
        SoundsController.PlayMainScreenBackgroundSound();        

        Time.timeScale = 0;
        Score = 0;

        HamsterGoBed.RestartTimer();
        OpenMainScreen();
        CloseGameScreen();
        CloseCompleteScreen();
        CloseLeaderboardScreen();
        ClosePauseScreen();
        CloseSettingsScreen();

        for (int i = 0; i < HamsterGoBed.SleepHamster.Count; i++)
        {

            HamsterGoBed.SleepHamster[i].RestartHamster();

        }
    }

    public int RecordScore(int pointsForPunch)
    {
        if (HamsterMove.b == true)
        {
            pointsForPunch = 20;
            Combo();
        }
        return Score += pointsForPunch;
    }

    public void PauseTheGame()
    {
        GameScreen.SetActive(true);
        Time.timeScale = 0;
        OpenPauseScreen();
        CloseGameScreen();
    }

    public void ContinueTheGame()
    {
        Time.timeScale = 1;
        ClosePauseScreen();
        OpenGameScreen();
    }


    public void OpenCompleteScreen()
    {
        CompleteScreen.SetActive(true);
    }

    public void CloseCompleteScreen()
    {
        CompleteScreen.SetActive(false);
    }

    public void OpenGameScreen()
    {
        GameScreen.SetActive(true);
    }

    public void CloseGameScreen()
    {
        GameScreen.SetActive(false);
    }

    public void OpenPauseScreen()
    {
        PauseScreen.SetActive(true);
    }

    public void ClosePauseScreen()
    {
        PauseScreen.SetActive(false);
    }

    public void OpenMainScreen()
    {
        MainScreen.SetActive(true);
    }

    public void CloseMainScreen()
    {
        MainScreen.SetActive(false);
    }

    public void OpenSettingsScreen()
    {
        SettingsScreen.SetActive(true);
    }

    public void CloseSettingsScreen()
    {
        SettingsScreen.SetActive(false);
    }

    public void OpenNewRecordScreen()
    {
        NewRecordScreen.SetActive(true);
    }

    public void CloseNewRecordScreen()
    {
        NewRecordScreen.SetActive(false);
    }

    public void OpenLeaderboardScreen()
    {
        LeaderboardScreen.SetActive(true);
    }

    public void CloseLeaderboardScreen()
    {
        LeaderboardScreen.SetActive(false);
    }


    public void Combo()
    {
        ComboIndicator.SetActive(true);
        isCombo = true;
        ShowComboTime = 0;
    }
}
