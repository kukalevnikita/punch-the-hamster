﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

using System.Linq;
using System.Text;


public class TimeController : MonoBehaviour
{

    float timerSec=0;

   

    public float DayTime;
    public int HowManyMinutesLeft;
    public int HowManySecondsLeft;

    public List<HamsterMove> SleepHamster;
    public HamsterMove hamsterMove;
    public UIController UiController;


    void Start()
    {
        HowManyMinutesLeft = (Convert.ToInt32(DayTime) / 60);
        HowManySecondsLeft = (Convert.ToInt32(DayTime) % 60);
    }

    void Update()
    {
        if (HowManyMinutesLeft >= 0)
        {
            timerSec += Time.deltaTime;
            if (timerSec > 1)
            {
                HowManySecondsLeft -= 1;
                timerSec = 0;
            }
            if (HowManySecondsLeft < 0 && HowManyMinutesLeft == 0)
            {
                HowManyMinutesLeft -= 1;
                HowManySecondsLeft = 0;
            }
            if (HowManySecondsLeft < 0)
            {
                HowManyMinutesLeft -= 1;
                HowManySecondsLeft = 60;
            }
        }


        

        UiController.GameTimer.text = ""+ HowManyMinutesLeft +":" + HowManySecondsLeft;

        
        if (HowManyMinutesLeft < 0)
        {
            
            for (int i = 0; i < SleepHamster.Count; i++)
            {
                SleepHamster[i].CurrentHamsterState = HamsterState.hamsterSleep;
            }

            int howHamstersInBox = 0;

            for (int i = 0; i < SleepHamster.Count; i++)
            {
                if (SleepHamster[i].transform.position == SleepHamster[i].PointInBox.transform.position)
                {
                    howHamstersInBox++;
                }
            }

            if (howHamstersInBox == SleepHamster.Count)
            {
                UiController.CloseGameScreen();
                UiController.OpenCompleteScreen();
            }
        }
    }

    public void RestartTimer()
    {
        HowManyMinutesLeft = (Convert.ToInt32(DayTime) / 60);
        HowManySecondsLeft = (Convert.ToInt32(DayTime) % 60);
    }
}
