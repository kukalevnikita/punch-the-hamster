﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class ScoreController : MonoBehaviour
{
    public GameObject FirstCoin;
    public GameObject SecondCoin;
    public GameObject ThirdCoin;

    [SerializeField] private int singleCoinScore;
    [SerializeField] private int twoCoinScore;
    [SerializeField] private int threeCoinScore;

    public bool isWindowsSetUp;

    public float Timer = 1.5f;
    public float Timer2 = 1.5f;
    public float Timer3 = 1.5f;

    public int BestScore;

    public UIController FinalScore;

    void OnEnable()
    {
        isWindowsSetUp = true;
    }

    void Start()
    {
        
    }

    void Update()
    {
        if (isWindowsSetUp == true)
        {
            ShowCoins();
        }
    }

    public void ShowCoins()
    {
        if (FinalScore.Score <= singleCoinScore)
        {
            
            Timer -= Time.deltaTime;
            if (Timer <= 0)
            {
                FirstCoin.SetActive(true);
                isWindowsSetUp = false;
                Timer = 1.5f;
            }

        }

        if (FinalScore.Score >= singleCoinScore)
        {
            Timer -= Time.deltaTime;
            if (Timer <= 0)
            {
                FirstCoin.SetActive(true);
                SecondCoin.SetActive(true);
                Timer = 1.5f;
                isWindowsSetUp = false;
            }
        }

        if (FinalScore.Score > twoCoinScore)
        {

            Timer -= Time.deltaTime;
            if (Timer <= 0)
            {
                FirstCoin.SetActive(true);
                SecondCoin.SetActive(true);
                ThirdCoin.SetActive(true);
                Timer = 1.5f;
                isWindowsSetUp = false;
            }
        }

    }
}
